import rekenen.*;
import rekenen.plugins.*;
import java.util.Scanner;

/**
 * PEER TUTORING
 * P2W3
 */
public class TestRekenmachine {
    private static Rekenmachine mijnCalc = new Rekenmachine();

    public static void main(String[] args) {
        //Opgave 3.1

        mijnCalc.installeer(new Optelling());
        mijnCalc.installeer(new Aftrekking());
        mijnCalc.installeer(new Vermenigvuldiging());
        mijnCalc.installeer(new Deling());
        mijnCalc.installeer(new Macht());
        mijnCalc.installeer(new Minimum());
        mijnCalc.installeer(new Maximum());

        //Opgave 3.3

        /*doeBerekeningEnDrukAf("+", 5, 2);
        doeBerekeningEnDrukAf("-", 5, 2);
        doeBerekeningEnDrukAf("*", 5, 2);
        doeBerekeningEnDrukAf("/", 5, 2);
        doeBerekeningEnDrukAf("^", 5, 2);
        doeBerekeningEnDrukAf("MIN", 5, 2);
        doeBerekeningEnDrukAf("MAX", 5, 2);
        doeBerekeningEnDrukAf("?", 5, 2);
        System.out.println(mijnCalc.toString());*/


        //Opgave 3.2

        System.out.println("Welkom bij het dynamische rekenmachine! ");
        boolean stoppen = false;

        while (!stoppen) {
            Scanner kb = new Scanner(System.in);
            System.out.println("Welke berekening wenst U uit te voeren (zie plugins, <ENTER> om te stoppen)? ");
            String bewerking = kb.nextLine();
            if (bewerking.isEmpty()) {
                stoppen = true;
                System.out.print(mijnCalc.getLog());
            } else {
                System.out.println(mijnCalc.toString());
                System.out.println("Geef ook twee getallen in (gescheiden door een spatie): ");
                double a = kb.nextDouble();
                double b = kb.nextDouble();
                System.out.printf("%.2f %s %.2f = %.2f %n %n", a, bewerking, b, mijnCalc.bereken(bewerking, a, b));
            }
        }
    }

    //Opgave 3.1
    private static void doeBerekeningEnDrukAf(String commando, double getal1, double getal2){
        System.out.printf("%f %s %f = %f\n"
                , getal1, commando, getal2
                , mijnCalc.bereken(commando, getal1, getal2));
    }


}
