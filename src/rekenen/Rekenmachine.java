package rekenen;

import rekenen.plugins.Plugin;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * PEER TUTORING
 * P2W3
 */
public class Rekenmachine {
    private final int MAX_AANTAL_PLUGINS = 10;
    private Plugin[] ingeladenPlugins;
    private int aantalPlugins;
    private StringBuilder log = new StringBuilder();

    public Rekenmachine() {
        this.ingeladenPlugins = new Plugin[MAX_AANTAL_PLUGINS];
        aantalPlugins = 0;
    }


    public void installeer(Plugin teInstallerenPlugin) {
        //Opgave 2.1.a
        boolean bestaatAl = false;
        if(aantalPlugins < this.MAX_AANTAL_PLUGINS){
            for(int i = 0; i < ingeladenPlugins.length; i++){
                if(ingeladenPlugins[i]!=null && ingeladenPlugins[i].equals(teInstallerenPlugin)){
                    bestaatAl = true;
                }
            }

            if(!bestaatAl){
                ingeladenPlugins[aantalPlugins] = teInstallerenPlugin;
                aantalPlugins++;
            }
        }
    }

    public double bereken(String command, double x, double y) {
        //Opgave 2.1.b
        double result = 0.0;
        for (Plugin plugin : ingeladenPlugins) {
            if (plugin != null && command.equals(plugin.getCommand())) {

                result = plugin.bereken(x, y);
                log.append("[").append(LocalDateTime.now().format(DateTimeFormatter.ofPattern("dd-MMM-yyyy HH:mm:ss"))).append("] ").append(String.format("%.2f %s %.2f = %.2f %n", x, command, y, result));
                return result;
            }
        }
        System.out.printf("Plugin %s niet geïnstalleerd.\n",command);
        return result;
    }

    public String getLog(){
        return "==== LOG ====\n" + log.toString();
    }

    @Override
    public String toString() {
        //Opgave 2.1c
        StringBuilder sb = new StringBuilder("Geinstalleerde Plugins: ");
        for (Plugin p : ingeladenPlugins) {
            if (p != null) {
                sb.append(p.getCommand() + " ");
            }
        }
        return String.format("%s", sb.toString());
    }
}
