package rekenen.plugins;

public class Minimum implements Plugin{
    @Override
    public String getCommand() {
        return "MIN";
    }

    @Override
    public double bereken(double x, double y) {
        return Math.min(x,y);
    }
}
