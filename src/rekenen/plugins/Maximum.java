package rekenen.plugins;

public class Maximum implements Plugin{
    @Override
    public String getCommand() {
        return "MAX";
    }

    @Override
    public double bereken(double x, double y) {
        return Math.max(x,y);
    }
}
